FROM docker.io/jenkins/jenkins:lts-jdk11
USER root
RUN apt-get -y update && \
 apt-get -y install apt-utils && \
 apt-get -y install apt-transport-https ca-certificates curl gnupg lsb-release && \
 curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
 echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
 apt-get -y update && \
 apt-get -y install docker-ce docker-ce-cli containerd.io && \
 apt-get clean && \
 rm -rf /var/lib/apt/lists/*
USER jenkins
